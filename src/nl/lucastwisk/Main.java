package nl.lucastwisk;

import nl.lucastwisk.objects.ComparisonItem;
import nl.lucastwisk.objects.MethodItem;
import nl.lucastwisk.objects.StepItem;
import nl.lucastwisk.resources.grammars.Java.JavaBaseListener;
import nl.lucastwisk.resources.grammars.Java.JavaLexer;
import nl.lucastwisk.resources.grammars.Java.JavaParser;
import nl.lucastwisk.resources.grammars.Java.JavaParser.*;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.Pair;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.runtime.tree.TerminalNodeImpl;
import org.antlr.v4.runtime.tree.Trees;
import sun.nio.cs.ext.COMPOUND_TEXT;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.*;

public class Main {

    private static final ArrayList<MethodItem> methods = new ArrayList<MethodItem>();

    private static final double DUPLICATION_THRESHOLD = 0.21;

    private static final int INSERT_COST = 1;
    private static final int REMOVE_COST = 1;
    private static final int UPDATE_COST = 1;

    private static final String METHOD_FEEDBACK_FORMAT = "The methods: '%s' (line %s-%s) and '%s' (line %s-%s) seem to contain duplicate code:\n";
    private static final String VALUE_FEEDBACK_FORMAT = "The values: '%s' (line %s) and '%s' (line %s) of type %s could be passed as a parameter to the method.";
    private static final String METHOD_NAME_FEEDBACK_FORMAT = "Think of a method name to replace both '%s' and '%s'";
    private static final String RETURN_TYPE_FEEDBACK_FORMAT = "Use a more generic return type to replace '%s' and '%s' with.";
    private static final String VARIABLE_NAME_FEEDBACK_FORMAT = "Think of a variable name to replace both variables '%s' (line %s) and '%s' (line %s)";

    public static void main(String[] args) {
        ANTLRInputStream inputStream = readFile("Exercise.java");
        System.out.print("(Step 1) Parsing Java file...");
        parseInputStream(inputStream);
        System.out.println(" Done!");
        System.out.println("(Step 2) Calculating normalized tree distance between tree pairs....");
        ArrayList results = calculateDistancesBetweenAllTrees(methods);
        System.out.println(" Done!");
        System.out.println("(Step 3) Extracting duplications...");
        ArrayList duplications = extractDuplicateMethods(results);
        produceFeedback(duplications);
    }

    private static ANTLRInputStream readFile(String fileName) {
        File file = new File(fileName);

        ANTLRInputStream charInputStream = null;
        FileInputStream fileStream;

        try {
            // Open the input file stream
            fileStream = new FileInputStream(file);
            // Open the ANTLR char inputstream
            charInputStream = new ANTLRInputStream(fileStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  charInputStream;
    }

    private static void parseInputStream(ANTLRInputStream inputStream) {

        Lexer lexer = new JavaLexer(inputStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        JavaParser parser = new JavaParser(tokens);

        JavaBaseListener listener = new JavaBaseListener() {

            @Override
            public void enterMethodDeclaration(JavaParser.MethodDeclarationContext ctx) {
                MethodItem method = new MethodItem(ctx);
                /* Collect all parseTrees of the methods */
                methods.add(method);
            }

        };

        ParserRuleContext t = parser.compilationUnit();
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(listener, t);
    }


    /* Iterate over al method pairs to calculate the tree distances */
    private static ArrayList<ComparisonItem> calculateDistancesBetweenAllTrees(ArrayList<MethodItem> methods) {

        ArrayList<Pair> pairs = getUniqueMethodPairs(methods);
        ArrayList<ComparisonItem> results = new ArrayList<ComparisonItem>();
        for (int i = 0; i < pairs.size(); i++) {

            printProgress((i + 1) / (double)pairs.size());

            Pair pair = pairs.get(i);
            MethodItem firstMethod = (MethodItem)pair.a;
            MethodItem secondMethod = (MethodItem)pair.b;

            int[][] treeEditDistance = calculateTreeEditDistance(firstMethod, secondMethod);
            results.add(new ComparisonItem(firstMethod, secondMethod, treeEditDistance));
        }
        return results;
    }

    /* Create all different method pairs*/
    private static ArrayList<Pair> getUniqueMethodPairs(ArrayList<MethodItem> methods) {

        ArrayList<Pair> pairs = new ArrayList<Pair>();

        for (int i = 0; i < methods.size(); i++) {
            for (int j = 0; j < methods.size(); j++) {
                MethodItem firstMethod = methods.get(i);
                MethodItem secondMethod = methods.get(j);
                if (!firstMethod.equals(secondMethod) && !pairs.contains(new Pair(secondMethod, firstMethod))) {
                    pairs.add(new Pair(firstMethod, secondMethod));
                }
            }
        }
        return pairs;
    }

    /* Create tree distance matrix for two methods*/
    private static int[][] calculateTreeEditDistance(MethodItem firstMethod, MethodItem secondMethod) {

        ArrayList<ParseTree> nodes1 = firstMethod.getOrderedNodes();
        ArrayList<Integer> kr1      = firstMethod.getKeyRoots();
        ArrayList<Integer> lmd1     = firstMethod.getLeftMostDescendants();
        ArrayList<String> labels1   = firstMethod.getLabels();

        ArrayList<ParseTree> nodes2 = secondMethod.getOrderedNodes();
        ArrayList<Integer> kr2      = secondMethod.getKeyRoots();
        ArrayList<Integer> lmd2     = secondMethod.getLeftMostDescendants();
        ArrayList<String> labels2   = secondMethod.getLabels();

        int[][] treeDistance    = new int[nodes1.size() + 1][nodes2.size() + 1];

        for (int i = 0; i < kr1.size(); i++) {
            for (int j = 0; j < kr2.size(); j++) {
                Integer keyRoot1 = kr1.get(i);
                Integer keyRoot2 = kr2.get(j);
                calculatePartialTreeDistance(treeDistance, keyRoot1, keyRoot2, lmd1, lmd2, labels1, labels2);
            }
        }
        return treeDistance;
    }

    /* Calculate tree distance between two subtrees with keyroots as rootnodes */
    private static void calculatePartialTreeDistance(int[][] treeDistance, int keyRoot1,
                                                  int keyRoot2, ArrayList<Integer> lmd1, ArrayList<Integer> lmd2,
                                                  ArrayList<String> labels1, ArrayList<String> labels2) {

        int m = keyRoot1 - lmd1.get(keyRoot1) + 2;
        int n = keyRoot2 - lmd2.get(keyRoot2) + 2;
        int[][] forestDistance = new int[m][n];

        int keyRootOffset1 = lmd1.get(keyRoot1) - 1;
        int keyRootOffset2 = lmd2.get(keyRoot2) - 1;

        /* Prepare forest distance table */
        for (int x = 1; x < m; x++) {
            forestDistance[x][0] = forestDistance[x - 1][0] + REMOVE_COST;
        }
        for (int y = 1; y < n; y++) {
            forestDistance[0][y] = forestDistance[0][y - 1] + INSERT_COST;
        }

        /* Fill tree distance table for all key root pairs*/
        for (int x = 1; x < m; x++) {
            for (int y = 1; y < n; y++) {

                /* Check if nodes are part of the tree */
                if (lmd1.get(x).equals(lmd1.get(x + keyRootOffset1)) && lmd2.get(y).equals(lmd2.get(y + keyRootOffset2))) {
                    int uCost = labels1.get(x).equals(labels2.get(y)) ? 0 : UPDATE_COST;
                    int remove = forestDistance[x - 1][y] + REMOVE_COST;
                    int insert = forestDistance[x][y - 1] + INSERT_COST;
                    int update = forestDistance[x - 1][y - 1] + uCost;
                    forestDistance[x][y] = Math.min(remove, Math.min(insert, update));
                    treeDistance[x+ keyRootOffset1][y + keyRootOffset2] = forestDistance[x][y];
                } else {

                    int p = lmd1.get(x + keyRootOffset1) - 1 - keyRootOffset1;
                    int q = lmd2.get(y + keyRootOffset2) - 1 - keyRootOffset2;

                    int remove = forestDistance[x - 1][y] + REMOVE_COST;
                    int insert = forestDistance[x][y - 1] + INSERT_COST;
                    int update = forestDistance[p][q] + treeDistance[x+ keyRootOffset1][y + keyRootOffset2];
                    forestDistance[x][y] = Math.min(remove, Math.min(insert, update));
                }
            }
        }
    }

    /* Filter normalized tree distances by threshold, to get the duplicates */
    private static ArrayList extractDuplicateMethods(ArrayList<ComparisonItem> results) {

        Collections.sort(results, new Comparator<ComparisonItem>() {
            @Override public int compare(ComparisonItem c1, ComparisonItem c2) {
                return Double.compare(c1.getNormalizedDistance(), c2.getNormalizedDistance());
            }
        });

        ArrayList duplicates = new ArrayList();
        for (ComparisonItem result : results) {
            MethodItem firstMethod = result.getFirstMethod();
            MethodItem secondMethod = result.getSecondMethod();

            if (result.getNormalizedDistance() < DUPLICATION_THRESHOLD) {
                duplicates.add(result);
            }
        }
        return duplicates;
    }

    /* Creates feedback for all duplicate methods found */
    private static void produceFeedback(ArrayList<ComparisonItem> duplicates) {
        String feedback = new String("********************\n***** FEEDBACK *****\n********************\n\n");

        for (ComparisonItem duplicate : duplicates) {

            MethodItem firstMethod = duplicate.getFirstMethod();
            MethodItem secondMethod = duplicate.getSecondMethod();

            feedback += String.format(METHOD_FEEDBACK_FORMAT, firstMethod.getIdentifier(), firstMethod.getStartLine(),
                    firstMethod.getEndLine(), secondMethod.getIdentifier(), secondMethod.getStartLine(), secondMethod.getEndLine());
            feedback += "------------------------------------------------------\n";
            feedback += "Here are some steps to remove the duplication:\n";

            for (StepItem step : duplicate.getSteps()) {
                String temp = feedbackForStep(step);
                feedback += temp != null ? String.format("- %s\n", temp) : "";
            }
            feedback += "\n\n";
        }
        System.out.println(feedback);
    }

    /* Produce feedback for the different steps (REMOVE, INSERT or SWAP)*/
    private static String feedbackForStep(StepItem step) {
        switch(step.getStepType()) {
            case REMOVE:
                return null;
            case INSERT:
                return null;
            case SWAP:
                return feedbackForSwap(step);
            default:
                return null;
        }
    }

    /* Produce feedback for the SWAP step */
    private static String feedbackForSwap(StepItem step) {
        ParseTree firstNode = step.getFirstNode();
        ParseTree secondNode = step.getSecondNode();
        // Dealing with leaves
        if (firstNode instanceof TerminalNodeImpl && secondNode instanceof  TerminalNodeImpl) {
            ParseTree firstParent = firstNode.getParent();
            ParseTree secondParent = secondNode.getParent();
            if (firstParent instanceof LiteralContext && secondParent instanceof  LiteralContext) {
                return feedbackForValueSwap(step);
            }
            else if (firstParent instanceof MethodDeclarationContext && secondParent instanceof MethodDeclarationContext) {
                return feedbackForMethodNameSwap(step);
            }
            else if (firstParent instanceof MethodDeclarationContext) {
                return feedbackForReturnTypeSwap(step);
            }
            else if (firstParent instanceof VariableDeclaratorIdContext && secondParent instanceof VariableDeclaratorIdContext) {
                return feedbackForVariableNameSwap(step);
            }
            else if (firstParent.getClass().equals(secondParent.getClass())) {

            }
        }
        return null;
    }

    /*Feedback for data duplication, depending on the type of the literal */
    private static String feedbackForValueSwap(StepItem step) {
        ParseTree firstNode = step.getFirstNode();
        ParseTree secondNode = step.getSecondNode();
        LiteralContext firstVariable = (LiteralContext)firstNode.getParent();
        LiteralContext secondVariable = (LiteralContext)secondNode.getParent();

        String type;
        if (firstVariable.StringLiteral() != null && secondVariable.StringLiteral() != null) {
            type = "String";
        }
        else if (firstVariable.IntegerLiteral() != null && secondVariable.IntegerLiteral() != null) {
            type = "Integer";
        }
        else if (firstVariable.FloatingPointLiteral() != null && secondVariable.FloatingPointLiteral() != null) {
            type = "Float";
        }
        else if (firstVariable.CharacterLiteral() != null && secondVariable.CharacterLiteral() != null) {
            type = "Character";
        }
        else if (firstVariable.BooleanLiteral() != null && secondVariable.BooleanLiteral() != null) {
            type = "Boolean";
        }
        else {
            type = "Unknown";
        }

        return String.format(VALUE_FEEDBACK_FORMAT, firstNode.getText(), firstVariable.getStart().getLine(),
                secondNode.getText(), secondVariable.getStart().getLine(), type);
    }

    /* Feedback for swap of method name */
    private static  String feedbackForMethodNameSwap(StepItem step) {
        return String.format(METHOD_NAME_FEEDBACK_FORMAT, step.getFirstNode().getText(), step.getSecondNode().getText());
    }

    /* Feedback for swap of return type */
    private static  String feedbackForReturnTypeSwap(StepItem step) {
        return String.format(RETURN_TYPE_FEEDBACK_FORMAT, step.getFirstNode().getText(), step.getSecondNode().getText());
    }

    /* Feedback for swap of variable name*/
    private static  String feedbackForVariableNameSwap(StepItem step) {
        VariableDeclaratorIdContext firstVariable = (VariableDeclaratorIdContext)step.getFirstNode().getParent();
        VariableDeclaratorIdContext secondVariable = (VariableDeclaratorIdContext)step.getSecondNode().getParent();
        return String.format(VARIABLE_NAME_FEEDBACK_FORMAT, step.getFirstNode().getText(), firstVariable.getStart().getLine(),
                step.getSecondNode().getText(), secondVariable.getStart().getLine());
    }

    //Print progress bar
    private static void printProgress(double progress) {
        String printString = "";
        int bars = (int)(progress * 20);
        for (int i = 0; i < 20; i++) {
            printString += i < bars ? "#" : "_";
        }
        System.out.printf("\r[%s] %s%%", printString, (int)(progress * 100));
    }

    private static void printDuplications(ArrayList<ComparisonItem> duplications) {

        for (ComparisonItem duplicate : duplications) {
            MethodItem firstMethod = duplicate.getFirstMethod();
            MethodItem secondMethod = duplicate.getSecondMethod();
            System.out.printf("%-30s ; %-30s ; %f\n", firstMethod.getIdentifier(),
                    secondMethod.getIdentifier(), duplicate.getNormalizedDistance());
        }
    }

    private static void printMatrix(int[][] matrix) {
        String printString = "\n";
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[0].length; col++) {
                printString += " " + String.format("%-2s", matrix[row][col]);
            }
            printString += "\n";
        }
        System.out.println(printString);
    }
}

