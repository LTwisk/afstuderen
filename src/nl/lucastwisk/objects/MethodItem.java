package nl.lucastwisk.objects;


import nl.lucastwisk.resources.grammars.Java.JavaParser;
import nl.lucastwisk.resources.grammars.Java.JavaParser.MethodDeclarationContext;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNodeImpl;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

/**
 * Created by Lucas on 23-05-16.
 */
public class MethodItem {

    private int startLine;
    private int endLine;
    private int length;
    private String identifier;
    private MethodDeclarationContext rootNode;

    private ArrayList<ParseTree>orderedNodes;
    private ArrayList<String> labels;
    private ArrayList<Integer> keyRoots;
    private ArrayList<Integer> leftMostDescendants;

    public MethodItem(MethodDeclarationContext ctx) {
        this.startLine = ctx.getStart().getLine();
        this.endLine = ctx.getStop().getLine();
        this.length = endLine - startLine;
        this.identifier = ctx.Identifier().getText();
        this.rootNode = ctx;

        this.orderedNodes = postOrderTraversal(ctx);
        this.labels = new ArrayList<String>();
        this.keyRoots = new ArrayList<Integer>();
        this.leftMostDescendants = new ArrayList<Integer>();
        createHelperTables();
    }

    /* Sorts nodes in post order traversal */
    private ArrayList<ParseTree> postOrderTraversal(ParseTree rootNode) {
        ArrayList<ParseTree> orderedNodes = new ArrayList<ParseTree>();
        postOrderTraversal(rootNode, orderedNodes);
        return orderedNodes;
    }

    /* Sort nodes of tree in post order traversal */
    private void postOrderTraversal(ParseTree node, ArrayList<ParseTree> orderedNodes) {
        if (node.getChildCount() == 0) {
            orderedNodes.add(node);
        } else {
            for (int i = 0; i < node.getChildCount(); i++) {
                postOrderTraversal(node.getChild(i), orderedNodes);
            }
            orderedNodes.add(node);
        }
    }

    /* Collects left most descendants and key roots */
    private void createHelperTables() {

        /* Add placeholder values because nodes are numbered from 1 instead of 0*/
        leftMostDescendants.add(0);
        labels.add("");

        for (int i = 0; i < orderedNodes.size(); i++) {
            ParseTree node = orderedNodes.get(i);
            String label;

            /* Use text as label when node is a terminal node, use class name when not.*/
            if (node instanceof TerminalNodeImpl) {
                label = node.getText();
            } else {
                label = node.getClass().toString();
            }
            labels.add(label);
            leftMostDescendants.add(orderedNodes.indexOf(getFirstLeaf(node)) + 1);
        }

        boolean[] visited = new boolean[leftMostDescendants.size()];
        Arrays.fill(visited, false);
        LinkedList<Integer> tempList = new LinkedList<Integer>();
        for (int i = leftMostDescendants.size() - 1; i > 0; i--) {
            if (!visited[leftMostDescendants.get(i)]) {
                tempList.addFirst(i);
                visited[leftMostDescendants.get(i)] = true;
            }
        }
        keyRoots.addAll(tempList);
    }

    /* Gets first leave for given node*/
    private static ParseTree getFirstLeaf(ParseTree node) {
        while (node.getChildCount() != 0) {
            node = node.getChild(0);
        }
        return node;
    }

    @Override
    public String toString() {
        return "Name:\t" + identifier + "\n"
                + "Start:\t" + startLine + "\n"
                + "End:\t" + endLine + "\n"
                + "Length:\t" + length;
    }

    public int getStartLine() {
        return startLine;
    }

    public int getEndLine() {
        return endLine;
    }

    public int getLength() {
        return length;
    }

    public String getIdentifier() {
        return identifier;
    }

    public MethodDeclarationContext getRootNode() {
        return rootNode;
    }

    public ArrayList<ParseTree> getOrderedNodes() {
        return orderedNodes;
    }

    public ArrayList<String> getLabels() {
        return labels;
    }

    public ArrayList<Integer> getKeyRoots() {
        return keyRoots;
    }

    public ArrayList<Integer> getLeftMostDescendants() {
        return leftMostDescendants;
    }
}
