package nl.lucastwisk.objects;

import org.antlr.v4.runtime.tree.ParseTree;

/**
 * Created by Lucas on 10-06-16.
 */
public class StepItem {

    public enum StepType{
        REMOVE, INSERT, SWAP
    }

    private ParseTree firstNode;
    private ParseTree secondNode;

    private StepType stepType;

    public StepItem(ParseTree firstNode, ParseTree secondNode, StepType stepType) {
        this.firstNode = firstNode;
        this.secondNode = secondNode;
        this.stepType = stepType;
    }

    public ParseTree getFirstNode() {
        return firstNode;
    }

    public ParseTree getSecondNode() {
        return secondNode;
    }

    public StepType getStepType() {
        return stepType;
    }
}
