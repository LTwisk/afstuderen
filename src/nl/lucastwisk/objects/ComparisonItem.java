package nl.lucastwisk.objects;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.Trees;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import java.lang.reflect.Method;
import java.util.ArrayList;

import static nl.lucastwisk.objects.StepItem.StepType.INSERT;
import static nl.lucastwisk.objects.StepItem.StepType.REMOVE;
import static nl.lucastwisk.objects.StepItem.StepType.SWAP;

/**
 * Created by Lucas on 07-06-16.
 */
public class ComparisonItem {

    private MethodItem firstMethod;
    private MethodItem secondMethod;

    private double normalizedDistance;
    private int[][] distanceTable;
    private ArrayList<StepItem> steps;

    public ComparisonItem(MethodItem firstMethod, MethodItem secondMethod, int[][] distanceTable) {
        this.firstMethod = firstMethod;
        this.secondMethod = secondMethod;
        this.distanceTable = distanceTable;
        this.normalizedDistance = normalizedTreeDistance();
        this.steps = extractSteps();
    }

    /* Normalise tree distance */
    private double normalizedTreeDistance() {
        ParseTree firstTree = firstMethod.getRootNode();
        ParseTree secondTree = secondMethod.getRootNode();

        int firstNodeCount = firstMethod.getOrderedNodes().size();
        int secondNodeCount = secondMethod.getOrderedNodes().size();
        double distance = distanceTable[firstNodeCount][secondNodeCount];

        return distance / (double)Math.max(firstNodeCount, secondNodeCount);
    }

    /* Method used to extract steps from the tree distance matrix that need to be taken to map two trees. */
    private ArrayList<StepItem> extractSteps() {

        ArrayList steps = new ArrayList<StepItem>();

        MethodItem firstMethod = getFirstMethod();
        MethodItem secondMethod = getSecondMethod();

        ArrayList<ParseTree> firstNodes = firstMethod.getOrderedNodes();
        ArrayList<ParseTree> secondNodes = secondMethod.getOrderedNodes();

        int firstNodeCount = firstNodes.size();
        int secondNodeCount = secondNodes.size();
        int lastDistance = distanceTable[firstNodeCount][secondNodeCount];

        int x = firstNodeCount;
        int y = secondNodeCount;

        while(x != 1 || y != 1) {
            int left     = (x - 1) == 0 ? Integer.MAX_VALUE : distanceTable[x - 1][y];
            int top      = (y - 1) == 0 ? Integer.MAX_VALUE : distanceTable[x][y - 1];
            int diagonal = (y - 1) == 0 || (x - 1 == 0) ? Integer.MAX_VALUE : distanceTable[x - 1][y - 1];

            ParseTree firstNode = firstNodes.get(x - 1);
            ParseTree secondNode = secondNodes.get(y - 1);

            if (left < diagonal && left < top) {
                x -= 1;
                lastDistance = left;
                steps.add(new StepItem(firstNode, secondNode, REMOVE));
            }
            else if (top < diagonal && top < left) {
                y -= 1;
                lastDistance = top;
                steps.add(new StepItem(firstNode, secondNode, INSERT));
            }
            else {
                x -= 1;
                y -= 1;

                if (diagonal < lastDistance) {
                    steps.add(new StepItem(firstNode, secondNode, SWAP));
                }
                lastDistance = diagonal;
            }


        }
        return steps;
    }

    public MethodItem getFirstMethod() {
        return firstMethod;
    }

    public MethodItem getSecondMethod() {
        return secondMethod;
    }

    public double getNormalizedDistance() {
        return normalizedDistance;
    }

    public int[][] getDistanceTable() {
        return distanceTable;
    }

    public ArrayList<StepItem> getSteps() {
        return steps;
    }
}
