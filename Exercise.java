    
/*
 * File: Yahtzee.java
 * ------------------
 * This program plays the Yahtzee game.
 */

import acm.io.*;
import acm.program.*;
import acm.util.*;

public class Yahtzee extends GraphicsProgram implements YahtzeeConstants 
{

    public static void main(String[] args)
    {
        new Yahtzee().start(args);
    }

    public void run()
    {
        IODialog dialog = getDialog();
        nPlayers = dialog.readInt("Enter number of players");
        playerNames = new String[nPlayers];
        for (int i = 1; i <= nPlayers; i++)
        {
            playerNames[i - 1] = dialog.readLine("Enter name for player " + i);
        }
        display = new YahtzeeDisplay(getGCanvas(), playerNames);
        categoryList = new int[nPlayers][N_CATEGORIES];
        playGame();

    }

        // Updates upper score total
    private void updateUpperScore() {
        display.updateScorecard(UPPER_SCORE, player, upperScore[player-1]);
        if (upperScore[player-1] > 62)
        {
            score = 35;
            display.updateScorecard(UPPER_BONUS, player, score);
        }
        else 
        {
            score = 0;
            display.updateScorecard(UPPER_BONUS, player, score);
        }
    }

    // Updates lower score total
    private void updateLowerScore() {
        display.updateScorecard(LOWER_SCORE, player, lowerScore[player-1]);
    }


    private void firstRoll(int playerNumber) 
    {
        for(int i = 0; i < N_DICE; i++) 
        {
            int rollDice = rgen.nextInt(MIN_DICE_VALUE, MAX_DICE_VALUE);
            roundScores[i] = rollDice;
        }
        display.printMessage(playerNames[playerNumber - 1] + "'s turn! Click the " + "\"Roll Dice\" " + "button to roll the dice.");
        display.waitForPlayerToClickRoll(playerNumber);
        display.displayDice(roundScores);
    }
    

    private void remainingRolls(int playerNumber) 
    {
        for (int i = 0; i < REMAINING_ROLLS; i++) 
        {
            display.printMessage("Select the dice you wish to re-roll and click " + "\"Roll Again\".");
            display.waitForPlayerToSelectDice();
            for(int j = 0; j < N_DICE; j++) 
            {
                if(display.isDieSelected(j))
                {
                    int rollDice = rgen.nextInt(MIN_DICE_VALUE, MAX_DICE_VALUE);
                    roundScores[j] = rollDice;
                }
            }
            display.displayDice(roundScores);
        }
    }


    // Conditions for three of a kind 
    private void threeOfaKindScore() {
        score = 0;
        checkHowMany();
        for (int i=0; i < diceSize; i++)
        {
            if (counter[i] > 2)
            {   
                summedUp();
                lowerScore[player-1] = lowerScore[player-1] + score;
            }       
        }
        makeShorter();
    }

    // Conditions for four of a kind
    private void fourOfaKindScore() {
        score = 0;
        checkHowMany();
        for (int i=0; i < diceSize; i++)
        {
            if (counter[i] > 3)
            {   
                summedUp();
                lowerScore[player-1] = lowerScore[player-1]+ score; 
            }
        }
        makeShorter();
    }


    private void largeStraightScore(){
        pointsOrNot = true;
        if(sorteddice[1] == 0){
            for(int i = 2 ; i <= amountOfEyes ;i++){
                if(sorteddice[i] == 0){
                    pointsOrNot= false;
                    break;
                }
            }
        }else if(sorteddice[amountOfEyes] == 0){
            for(int i = 1 ; i < amountOfEyes ;i++){
                if(sorteddice[i] == 0){
                    pointsOrNot= false;
                    break;
                }
            }
        }
    }
    private void smallStraightScore(){
        // checks wether 4 subsequent amount of eyes all have been thrown
        pointsOrNot = true;
        for(int i = 1 ; i < 4 ; i++){
            pointsOrNot = true;
            for(int y = i; y < i + 4; y++){
                if(sorteddice[y] == 0){
                    pointsOrNot = false;
                }
            }
            if(pointsOrNot == true){
                break;
            }
        }
    }

        private boolean checkSmallStraight(int dice[])
    {
        streetCount = 1;
        smallest = 6;

        for(int i=0; i < N_DICE; i++)
        {
            if(dice[i] <= smallest)
            {
                smallest = dice[i];
            }
        }

        for(int i=0; i < N_DICE - 1; i++)
        {
            checkrow(smallest, dice);
        }
        
        if(streetCount == 4)
        {
            return true;
        }
        return false;
    }

    // same as small straight, but returns true if streetCount is 5.
    private boolean checkLargeStraight(int dice[])
    {
        streetCount = 1;
        smallest = 6;

        for(int i=0; i < N_DICE; i++)
        {
            if(dice[i] <= smallest)
            {
                smallest = dice[i];
            }
        }

        for(int i=0; i < N_DICE; i++)
        {
            checkrow(smallest, dice);
        }
        
        if(streetCount == 5)
        {
            return true;
        }
        return false;
    }

        private int scoreGeneration(int location) {
        usedCategories.add(location);
        int totalScore;
        if (fitsCategory(location)) {
            if (location < 7) {
                totalScore = location * countOccurence(location);
                return(totalScore);
            } else {
                switch (location) {
                    case THREE_OF_A_KIND:
                        return(computeTotal());
                
                    case FOUR_OF_A_KIND:
                        return(computeTotal());
                
                    case FULL_HOUSE:
                        return(25);
                
                    case SMALL_STRAIGHT:
                        return(30);
                
                    case LARGE_STRAIGHT:
                        return(40);
                
                    case YAHTZEE:
                        return(50);
                    
                    case CHANCE:
                        return(computeTotal());
                    
                    default: 
                        return(2);
                }
            }
        } else {
            return(0);
        }    
    } 
    
    // Gives a value on whether the roll fits into the category clicked.
    private boolean fitsCategory(int location) {
        boolean p = false;
        if (location < 7 || location == CHANCE) {
            return(true);
        } else {
        switch (location) {
                case THREE_OF_A_KIND: 
                    p = isThreeOfAKind();
                    break;
                    
                case FOUR_OF_A_KIND:
                    p = isFourOfAKind();
                    break;
                    
                case FULL_HOUSE:
                    p = isFullHouse();
                    break;
                
                case SMALL_STRAIGHT:
                    p = isSmallStraight();
                    break;
                    
                case LARGE_STRAIGHT:
                    p = isLargeStraight();
                    break;
                    
                case YAHTZEE:
                    p = isYahtzee();
                    break;
       }         
            if (p) {
                return(true);
            } else {
                return(false);
            }       
        }
    }

        private int ones(int dice[])
    {
        int score = 0;
        for(int i=0; i<N_DICE; i++)
        {
            if(dice[i] == 1)
            {
                score += 1;
            }
        }
        return score;
    }

    private int twos(int dice[])
    {
        int score = 0;
        for(int i=0; i<N_DICE; i++)
        {
            if(dice[i] == 2)
            {
                score += 2;
            }
        }
        return score;
    }

    private void rerollMessage()
    {
        display.printMessage("Select the dice you wish to re-roll and click ");
    }

    private void selectCategoryMessage()
    {
        display.printMessage("Select a category for this roll.");
    }


}

